/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 18.11.21 
*дата останньої зміни 18.11.21 
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для обробки елементів у визначеній частині матриці відповідно
індивідуального завдання, що знаходиться у таблиці
*призначення програмнного файлу: Усі елементи у зафарбованій частині матриці а9(n,n)
замінити максимальним елементом всієї матриці.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j , n,m , max=0;
	srand(time(NULL));
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a9[n][m];
	printf("Матриця a9( %d, %d): \n",n,m);
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			a9[i][j] = rand()%100-50;
			printf("%4d", a9[i][j]);
			if(max < a9[i][j])
				max = a9[i][j];
			}
		cout<<endl;
		}
	cout<<"MAX = "<<max<<endl;
	for (j = 0; j < m; j++){
		for (i = j; i < n; i++){
			if ( ( (i+j)/2 ) < ( ( m+n ) /4 ) )
			a9[i][j]=max; 
		}
 	}
	cout<<"Вихідна матриця"<<endl;
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			printf("%4d", a9[i][j]);
		}
		cout<<endl;
		}
	cout<<endl;
	system("pause");

	return 0;
}
